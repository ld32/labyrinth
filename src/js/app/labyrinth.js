/**
 * Created by ascet on 25.02.16.
 */

var $cursor = $('<img src="/images/interface/cursor.png" class="cursor">');
var $start = $('<img src="/images/objects/start.png" class="start" column=0 row=0>');
var $finish = $('<img src="/images/objects/finish.png" class="finish" column=0 row=0>');
var way = [];
var states = {
    start : 'start',
    finish : 'finish',
    wall : 'wall'
};
var possibleSteps = {
    left :  { column : -1, row:0 },
    right : { column : +1, row:0 },
    up :    { column : 0, row:-1 },
    down :  { column : 0, row:+1 }
};
var active = true;

var stepImages = [
    { image:'/images/way/finish_down.png', from: possibleSteps.down, to: states.finish },
    { image:'/images/way/finish_left.png', from: possibleSteps.left, to: states.finish },
    { image:'/images/way/finish_right.png', from: possibleSteps.right, to: states.finish },
    { image:'/images/way/finish_up.png', from: possibleSteps.up, to: states.finish },
    { image:'/images/way/go_down_left.png', from: possibleSteps.down, to: possibleSteps.left },
    { image:'/images/way/go_down_right.png', from: possibleSteps.down, to: possibleSteps.right },
    { image:'/images/way/go_up_left.png', from: possibleSteps.up, to: possibleSteps.left },
    { image:'/images/way/go_up_right.png', from: possibleSteps.up, to: possibleSteps.right },
    { image:'/images/way/go_vertical.png', from: possibleSteps.up, to: possibleSteps.down },
    { image:'/images/way/go_horizontal.png', from: possibleSteps.left, to: possibleSteps.right },
    { image:'/images/way/start_down.png', from: states.start, to: possibleSteps.down },
    { image:'/images/way/start_left.png', from: states.start, to: possibleSteps.left },
    { image:'/images/way/start_right.png', from: states.start, to: possibleSteps.right },
    { image:'/images/way/start_up.png', from: states.start, to: possibleSteps.up }
];

var wallImages = [
    '/images/landscape/wall_0.png',
    '/images/landscape/wall_1.png',
    '/images/landscape/wall_2.png',
    '/images/landscape/wall_3.png',
    '/images/landscape/wall_4.png'
];

/**
 * set way to an array of null size of width to height
 * @param {number} width - Width of a labyrinth.
 * @param {number} height - Height of a labyrinth.
 */
function rebuildWay(width, height) {
    way = [];
    for (var i = 0; i < width; i++) {
        var line = [];

        for (var j = 0; j < height; j++) {
            line.push(null);
        }

        way.push(line);
    }
}

/**
 * generates the maze, which will be of a array of 0 (blank) and 1 (Wall)
 * @param {number} width - Width of a labyrinth.
 * @param {number} height - Height of a labyrinth.
 * @param {number} density - the ratio of the walls to the empty space as a percentage.
 * 20 means that the wall will be five times smaller than the empty spaces.
 * @return {number[][]} Generated labyrinth.
 */
function generateLabyrinth(width, height, density) {
    if ((density === undefined) || (density == null)) {
        density = 20;
    }

    rebuildWay(width, height);

    var map = [];
    for (var i = 0; i < width; i++) {
        var line = [];

        for (var j = 0; j < height; j++) {
            var point = (Math.random() * 100 > density) ? 0 : 1;

            line.push(point);
        }

        map.push(line);
    }

    return map;
}

/**
 * Rendering a labyrinth in this div for a given array, which is generated GenerateLabyrinth()
 * @param {Object} $container - div for render.
 * @param {number[][]} map - array of labyrinth
 */
function renderMap($container, map) {
    $container.empty();

    for (var column in map) {
        for (var row in map[column]) {
            var $space = $("<img class='field space' src='/images/landscape/space.png'>");

            $space.attr('id', 'field_c' + column + 'r' + row);
            $space.attr('style', 'left:' + (column * 100) + 'px;top:' + (row * 100) + 'px;');
            $space.attr('column', column);
            $space.attr('row', row);
            
            $container.append($space);
            
            if (map[column][row] == 1) {
                var image = wallImages[Math.round(Math.random()*(wallImages.length-1))];

                var $wall = $("<img class='field wall' src='" + image + "'>");

                $wall.attr('id', 'field_c' + column + 'r' + row);
                $wall.attr('style', 'left:' + (column * 100) + 'px;top:' + (row * 100) + 'px;');
                $wall.attr('column', column);
                $wall.attr('row', row);
                $container.append($wall);
                
                way[column][row] = states.wall;
            }
        }
    }
}
/**
 * change item on way array
 * @param {number} column
 * @param {number} row
 * @param {*} state - new state of item
 */
function changeWayItem(column, row, state) {
    way[column][row] = state;
}

function isOutFromMap(i, j) {
    return (i < 0) || (j < 0) || (i >= way.length) || (j >= way.length);
}

function isValidToStep(i, j) {
    return !isOutFromMap(i, j) && ((way[i][j] == null) || (way[i][j] == states.finish));
}

function step(column, row, range) {
    if (way[column][row] != range) {
        return false;
    }
    
    var stepped = false;

    for (var step in possibleSteps) {
        var newColumn = column + possibleSteps[step].column;
        var newRow = row + possibleSteps[step].row;

        if (isValidToStep(newColumn, newRow)) {
            changeWayItem(newColumn, newRow, range + 1);
            stepped = true;
        }
    }
    
    return stepped;
}

function isFinished() {
    return way[$finish.attr('column')][$finish.attr('row')] != states.finish;
}

function findWay(range) {
    if (!$start.is(":visible") || !$finish.is(":visible")) {
        return;
    }

    range = (range !== undefined) ? range : 0;

    changeWayItem($start.attr('column'), $start.attr('row'), 0);
    
    var stepCount = 0;

    for (var column in way) {
        for (var row in way[column]) {            
            if (step(parseInt(column), parseInt(row), range)) {
                stepCount++;
            }
        }
    }
    
    if (!isFinished() && (stepCount > 0)) {
        findWay(range + 1);
    } else {
        clearWay(range + 1, [
            {
                column: parseInt($finish.attr('column')),
                row: parseInt($finish.attr('row'))
            }
        ]);
    }
}

function getNeighbors(items, range) {
    var neighbors = [];

    items.forEach(function (item) {
        for (var step in possibleSteps) {
            var newColumn = item.column + possibleSteps[step].column;
            var newRow = item.row + possibleSteps[step].row;

            if (!isOutFromMap(newColumn, newRow) && (way[newColumn][newRow] == range)) {
                neighbors.push({
                    column: newColumn,
                    row: newRow
                })
            }  
        }
    });

    return neighbors;
}

function clearWay(range, currentSteps) {
    for (var column in way) {
        for (var row in way[column]) {
            if ((way[column][row] == range) && (needToClear(column, row, currentSteps))) {
                way[column][row] = null;
            }
        }
    }

    if (range != 0) {
        clearWay(range - 1, getNeighbors(currentSteps, range - 1));
    }
}

function needToClear(column, row, excludingList) {
    return !excludingList.find(function(item) {
        return (item.column == column) && (item.row == row);
    })
}

function renderWay() {
    for (var column in way) {
        for (var row in way[column]) {
            if (isStep(parseInt(column), parseInt(row))) {
                drawStep(parseInt(column), parseInt(row));
            }
        }
    }
}

function drawStep(column, row) {
    for (var i in stepImages) {
        var direction = stepImages[i];
        
        if (direction.from == states.start) {
            var toColumn = column + direction.to.column;
            var toRow = row + direction.to.row;
            
            if (isStart(column, row) && isStep(toColumn, toRow)) {
                addStep(column, row, direction.image);
            }
            
           continue; 
        }

        if (direction.to == states.finish) {
            var fromColumn = column + direction.from.column;
            var fromRow = row + direction.from.row;

            if (isFinish(column, row) && isStep(fromColumn, fromRow)) {
                addStep(column, row, direction.image);
            }

            continue;
        }
        
        var fromColumn = (direction.from != states.start) ? column + direction.from.column : column;
        var fromRow = (direction.from != states.start) ? row + direction.from.row : row;
        
        var toColumn = (direction.to != states.finish) ? column + direction.to.column : column;
        var toRow = (direction.to != states.finish) ? row + direction.to.row : row;
        
        if (isStep(fromColumn, fromRow) && isStep(toColumn, toRow)) {
            addStep(column, row, direction.image);
        }
    }
}

function addStep(column, row, image) {
    var $image = $('<img src="'+ image +'" class="step">');

    $image.attr('style', 'left:' + (column * 100) + 'px;top:' + (row * 100) + 'px;');

    $('#map').append($image);
}

function isStep(column, row) {
    return !isOutFromMap(column, row) && (way[column][row] != null) && (way[column][row] != states.wall);
}

function isStart(column, row) {
    return ($start.attr('column') == column) && ($start.attr('row') == row);
}

function isFinish(column, row) {
    return ($finish.attr('column') == column) && ($finish.attr('row') == row);
}

renderMap($('#map'), generateLabyrinth(5, 5));

var currentState = states.start;

$('#map').append($cursor);
$('#map').append($start);
$('#map').append($finish);

function moveCursor(event) {
    var top = $(event.toElement).position().top;
    var left = $(event.toElement).position().left;

    $cursor.attr('style', 'top:' + top + 'px;left:' + left + 'px;');
    $cursor.attr('field-id', $(event.toElement).attr('id'));
}

$('.space').mousemove(moveCursor);

$('.cursor').click(function (event) {
    if (active) {
        var $field = $('#' + $(event.toElement).attr('field-id'));

        if (!$field.hasClass('wall') && !$field.hasClass('start') && !$field.hasClass('finish')) {
            var top = $field.position().top;
            var left = $field.position().left;

            var $standingObject = (currentState == states.start) ? $start : $finish;

            changeWayItem($standingObject.attr('column'), $standingObject.attr('row'), null);
            changeWayItem($field.attr('column'), $field.attr('row'), currentState);

            $standingObject.attr('style', 'top:' + top + 'px;left:' + left + 'px;');
            $standingObject.attr('column', $field.attr('column'));
            $standingObject.attr('row', $field.attr('row'));

            $standingObject.show();

            currentState = (currentState == states.start) ? states.finish : states.start;
        }
    }
});

$('.find_way').click(function () {
    findWay();
    renderWay();
    active = false;
});

$('.new_map').click(function () {
    $start.hide();
    $finish.hide();

    $('.space').detach();
    $('.wall').detach();
    $('.step').detach();

    $('#map').empty();
    
    renderMap($('#map'), generateLabyrinth(5, 5));

    currentState = states.start;

    $('#map').append($cursor);
    $('#map').append($start);
    $('#map').append($finish);

    $('.space').mousemove(moveCursor);
    
    active = true;
});
