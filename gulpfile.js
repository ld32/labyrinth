/**
 * Created by ascet on 25.02.16.
 */
var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

gulp.task('default', function() {
    gulp.src([
            "./src/js/vendor/jquery.min.js",
            "./src/js/app/labyrinth.js"
        ])
        .pipe(concat('app.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./src/js/'));
});
